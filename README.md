## MindsGaming Linux APP

---
A basic linux app for our website located on [https://mindsgaming.glitch.me](https://mindsgaming.glitch.me)

# Install

**Gab the Git:**

`git clone https://gitlab.com/mindsgaming-mg/mindsgaming-desktop-app.git`


**Open The Folder In Terminal **

`cd mindsgaming-desktop-app`

**Run The App**

`#MindsGaming`
